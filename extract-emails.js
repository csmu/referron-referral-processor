#!/usr/bin/env node

var commandLineArguments = require('minimist')(process.argv.slice(2)) ;
var fs = require('fs') ;
var path = require('path') ;

function harvestEmailAddresses( error, fileNames ) {
	//console.dir(directory) ;
	emails = {} ;
	countEmails = 0 ;
	fromCount = {} ;
	toCount = {} ;
	ccCount = {} ;
	exceptions = [] ;
	for ( fileNameIndex = 0 ; fileNameIndex < fileNames.length; fileNameIndex++){
		var filePath = path.join( directory, fileNames[fileNameIndex] ) ;
		// console.dir(filePath) ;
		try {
			var json = JSON.parse(fs.readFileSync(filePath, 'utf8'));
			body = json.body ;
			if ( body ) {
				from = body.from ;
				if ( from ) {
					if ( !from.name ) {
						from.name = from.address.toLowerCase();
					}
					emails[from.address.toLowerCase()] = from.name ;
					count = fromCount[from.address] ;
					if (!count) {
						fromCount[from.address] = 1 ;
					} else {
						fromCount[from.address] = count + 1 ;
					}
				}
				toRecipients = body.to ;
				for ( toIndex = 0 ; toIndex < toRecipients.length ; toIndex++ ) {
					var to = toRecipients[toIndex] ;
					if ( !to.name ) {
						to.name = to.address;
					}
					emails[to.address.toLowerCase()] = to.name ;
					count = toCount[to.address] ;
					if (!count) {
						toCount[to.address] = 1 ;
					} else {
						toCount[to.address] = count + 1 ;
					}
				}
				ccRecipients = body.cc ;
				for ( ccIndex = 0 ; ccIndex < ccRecipients.length ; ccIndex++ ) {
					var cc = ccRecipients[ccIndex] ;
					if ( !cc.name ) {
						cc.name = cc.address;
					}
					emails[cc.address.toLowerCase()] = cc.name ;
					count = ccCount[cc.address] ;
					if (!count) {
						ccCount[cc.address] = 1 ;
					} else {
						ccCount[cc.address] = count + 1 ;
					}
				}
			}
			countEmails++ ;
		} catch ( exception ) {
			exceptions.push( {exception:exception,filePath:filePath}) ;
		}
	}
	delete emails['referral@referron.com'] ;
	delete emails['referrals@app.referron.com'] ;
	ordered = [] ;
	for ( key in emails ) {
		ordered.push(key) ;
	}
	orderedEmails = [] ;
	ordered.sort() ;
	for ( orderedIndex = 0 ; orderedIndex < ordered.length ; orderedIndex++){
		key = ordered[orderedIndex];
		value = emails[key] ;
		countFrom = fromCount[key] ;
		countTo = toCount[key] ;
		countCc = ccCount[key] ;
		if ( !countFrom ) {
			countFrom = 0 ;
		}
		if ( !countTo ) {
			countTo = 0 ;
		}
		if ( !countCc ) {
			countCc = 0 ;
		}
		orderedEmails.push( {name:value,email:key,fromCount:countFrom,toCount:countTo,ccCount:countCc});
	}
	orderedEmails.sort( function(a,b){
		return a.name.toLowerCase().localeCompare(b.name.toLowerCase()) ;
	});
	
	results.referralsCount = countEmails ;
	results.filesCount = fileNames.length ;
	results.emailAddressesCount = orderedEmails.length ;
	results.emailAddresses = orderedEmails ;
	results.exceptions = exceptions ;
	fs.writeFile( output, JSON.stringify(results), function(error) {
	    if(error) {
				console.dir(error)
	    } else {
				console.dir(results) ;
				console.log( output + " written."); 	
				text = "name\temail\tfromCount\ttoCount\tccCount" ;
				for( orderedEmailIndex = 0 ; orderedEmailIndex < orderedEmails.length; orderedEmailIndex++){
					orderedEmail = orderedEmails[orderedEmailIndex] ;
					text += '\n' 
						+ orderedEmail.name + '\t' 
						+ orderedEmail.email + '\t' 
						+ orderedEmail.fromCount + '\t'
						+ orderedEmail.toCount + '\t'
						+ orderedEmail.ccCount 
					;
				}
				
				fs.writeFile( tabbed, text, function(error){
					if (error){
						console.dir(error)
					} else {
						console.log( tabbed + " written."); 
						console.dir( {referralsCount:countEmails, filesCount:fileNames.length, emailAddressesCount:orderedEmails.length}); 
					}
						
				}) ;
				
	    }
	}); 
}

var errors = [] ;
var results = {} ;
var directory = commandLineArguments.directory ;
var output = commandLineArguments.output ;
var tabbed = commandLineArguments.tabbed ;

if ( !directory ) {
	errors.push( {error:"directory parameter is required"} ) ;
}
if ( !output ) {
	errors.push( {error:"output parameter is required"} ) ;
}
if ( !tabbed ) {
	errors.push( {error:"tabbed parameter is required"} ) ;
}

if ( errors.length > 0 ) {
	console.dir( errors ) ;	
} 
else 
{
	results['directory'] = directory ; 
	results['output'] = output ;
	results['tabbed'] = tabbed ;
	
	fs.readdir(directory, harvestEmailAddresses ) ;	
}
