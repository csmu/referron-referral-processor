#!/usr/bin/env node

var commandLineArguments = require('minimist')(process.argv.slice(2));
var fs = require('fs');
var validMXDomain = require('legit');
var notifier = require('mail-notifier');
var uuid = require('node-uuid');
var request = require('request');
var SparkPost = require('sparkpost');
var sprintf = require("sprintf-js").sprintf,
    vsprintf = require("sprintf-js").vsprintf

var config = commandLineArguments.config;

if (!config) {
	config = 'config.json';
}
config = JSON.parse(fs.readFileSync(config, 'utf8'));

var mailHost = null;
var sparkPost = new SparkPost(config.sparkpostApiKey);

function sendSparkPostMessage(sparkPost_, from_, subject_, body_, recipients_) {

	if (typeof body_ == 'json_object') {
		body_ = JSON.stringify(body_);
	}

	sparkPost_.transmissions.send({
		transmissionBody: {
			content: {
				from: from_,
				subject: subject_,
				html: body_
			},
			recipients: recipients_
		}
	}, function(err, res) {
		if (err) {
			console.log('Unexpected error sending email notification');
			console.log(err);
			//console.dir({
			//	body_: body_
				//});
			if (err.errors) {
				if (err.errors[0]) {
					if (err.errors[0].code == '1300') {
						console.log('trying sendSparkPostMessageJsonBodyAsText')
						sendSparkPostMessageJsonBodyAsText(sparkPost_, from_, subject_, body_, recipients_)
					}
				}
			}
		} else {
			console.dir({
				sent: subject_,
				'to': recipients_
			});
		}
	});
}

function sendSparkPostMessageJsonBodyAsText(sparkPost_, from_, subject_, body_, recipients_) {

	body_ = JSON.stringify(body_);

	sparkPost_.transmissions.send({
		transmissionBody: {
			content: {
				from: from_,
				subject: subject_,
				text: body_
			},
			recipients: recipients_
		}
	}, function(err, res) {
		if (err) {
			console.log('Unexpected error sending email notification');
			console.log(err);
			console.dir({
				body_: body_
			});
		} else {
			console.dir({
				sent: subject_ ,
				'to': recipients_
			});
		}
	});


}

function logonToReferron(options_, callback_) {

	process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
	var error = null ;
	
	// console.log('attempting to logon');
	// console.dir( {options_: options_} ) ;
	
	request({
		url: options_.url,
		method: 'POST',
		json: { email: options_.email, password: options_.password }
	}, function(error, response, body) {
		// console.dir( {logonToReferron: { error: error, response: response.headers, body: body } } ) ;
		if (!error){
			options_['authorization-token'] = response.headers['authorization-token'] ;
			if (!options_['authorization-token']){
				error = {error:"no authorization-token", headers: response.headers } ;
			}
		}
		callback_( error, options_);
	});

}

function logonToReferronProcess(error,referron) {

				if (error) {
					var noAuthorisationToken = false ;
					if ( error.error == 'no authorization-token') {
						noAuthorisationToken = true ;
						console.log( dateFormatAsString(new Date(), 'yyyyMMdd.hhmm') + ': attempting logon again ');
						logonToReferron(referron, logonToReferronProcess );
					} 
					else 
					{
						/* some other error */
						sendSparkPostMessage(sparkPost, config.notify.sparkpost.from, 'Unexpected error', JSON.stringify({
							error: error
						}), config.notify.sparkpost.recipients);
					}
				
				} else {

					console.log( dateFormatAsString(new Date(), 'yyyyMMdd.hhmm') + ': Starting listening to ' + referron.imap.user + ' on ' + referron.imap.host);
					console.dir( { 'authorization-token': referron['authorization-token'] }) ;
				
					var n = notifier(referron.imap);
					n.on('end', function() { // session closed
						n.start();
					}).on('error', function(err) {
						console.dir(err);
					}).on('mail', function(m) {
						processReferrals(m,referron) ;	
					}).start();

				}
}

/*
 * http://stackoverflow.com/questions/23593052/format-javascript-date-to-yyyy-mm-dd
 */
dateFormatAsString = function date2str(x, y) {
    var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function(v) {
        return x.getFullYear().toString().slice(-v.length)
    });
}

function processReferrals(m,referron) {
	
	/*
		if no to address put the sender in the to
	*/
	if ( !m.to ) {
		// m.to = m.from ;
	}
	/*
		if only one to address and no cc put the sender in the cc
	*/
	if ( m.to.length == 1 && !m.cc ) {
		// m.cc = m.from ;
		m.cc - m.from ;
	}
	
	if (m.to) {
		
		var providers = [] ;
		var seekers = [] ;
		var ccs = [] ;
		var referralAtReferronFound = false ;
	
		ccs.push({address:'referral@referron.com',name:'Referral Processor'}) ;
		if ( m.cc ) 
		{
			for ( var ccIndex = 0; ccIndex < m.cc.length; ccIndex++ ){
				var provider = m.cc[ccIndex] ;
				if ( provider.address.toLowerCase() ==  'referral@referron.com' ) {
					/* ignore */
					referralAtReferronFound = true ;
				} else {
					providers.push( m.cc[ ccIndex ] ) ;
				}
			}
		} // test for message cc
	
		if ( providers.length == 0 ) {
			/* the first to address will be the provider, the rest will be seekers */
			for ( var toIndex = 0; toIndex < m.to.length ; toIndex++ ){
				var provider = m.to[toIndex] ;
				if ( provider.address ) {
					if ( provider.address.toLowerCase()  ==  'referral@referron.com' ) {
						/* ignore */
						referralAtReferronFound = true ;
					} else {
						if ( providers.length == 0) {
							providers.push( m.to[ toIndex ] ) ;
						} else {
							seekers.push( m.to[ toIndex ] ) ;
						}
						m.to.splice(toIndex, 1);
					}
				}
			}
		} else {
			for ( var toIndex = 0; toIndex < m.to.length; toIndex++ ){
				var seeker = m.to[toIndex] ;
				if ( seeker.address.toLowerCase()  ==  'referral@referron.com' ) {
					/* ignore */
					referralAtReferronFound = true ;
				} else {
					seekers.push( m.to[ toIndex ] ) ;
				}
			}
		}
	
		console.dir( {now: dateFormatAsString(new Date(), 'yyyyMMdd.hhmm'), from: m.from, providers: providers, seekers: seekers }) ;
	
		for ( var providerIndex = 0; providerIndex < providers.length; providerIndex++)
		{
		
			var provider = providers[providerIndex] ;
		
			for ( var seekerIndex = 0 ; seekerIndex < seekers.length; seekerIndex++)
			{
			
				var seeker = seekers[seekerIndex] ;
				if ( seeker.address.toLowerCase() == provider.address.toLowerCase() ) 
				{
					/* 
						ignore as we do not want refer someone to themselves.
					*/
				}
				else 
				{
				
					var to = [] ;
					to.push(seeker) ;
					to.push(provider) ;


					
					console.dir(to) ;
					
					var uuidV4 = uuid.v4();
					var date = m.date;
					var from = m.from[0];
					var to = to;
					var cc = ccs;
					var subject = m.subject;
					var text = m.text;
					var html = m.html;

	
					var webhook = config.webhook;
					webhook.body.uuid = uuidV4;
					webhook.body.date = date;
					webhook.body.from = from;
					webhook.body.to = to;
					webhook.body.cc = cc;
					webhook.body.subject = subject;
					webhook.body.text = text;
					webhook.body.html = html;
					webhook.message = m ;

					// console.log('... webhook.start ...');
					// console.dir(JSON.stringify(webhook));
					//console.log('...') ;
					//console.dir(m.headers) ;
					// console.log('... webhook.end ...');

					var url = webhook.url;
					/* remove for production */
					process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
					var now = new Date().toISOString().
						replace(/Z/g,'').
						replace(/T/g, '.'). // replace T with a .
						replace(/:/g, '').  // replace : with nothing
					replace(/-/g,'') ; 	// rplace - with nothing
	
					var path = "referrals/" + now + '-' + webhook.body.from.name.replace(/ /g,'-') + '-' + webhook.body.from.address + '-' + webhook.body.uuid + '.json' ;
					fs.writeFile( path, JSON.stringify(webhook), function(err) {
					    if(err) {
								sendSparkPostMessage(sparkPost, config.notify.sparkpost.from, 'Unexpected error', JSON.stringify({
									error: err
								}), config.notify.sparkpost.recipients);
					    } else {
								console.log( path + " written."); 	
					    }
					}); 

					if( webhook.body.subject.toLowerCase().split('re:').length > 1 && referralAtReferronFound ) 
					{
						// console.log('... sending Referral Not Recorded start ... ');			
						var recipients = [] ;		
						var htmlTemplate = "Hi %s<br/><br/>Your referral of %s %s to %s %s with the subject %s has <b>NOT</b> been recorded because it looks like it a reply to a referral.<br/><br/>If this was a real referral please send again <b>after either removing 'Re:' from the subject or move referral@referron.com to the bcc field</b>.<br/><br/>Regards<br/><br/>Your Referral Team at http://referron.com";
						var confirmation = vsprintf(htmlTemplate, 
							[	from.name, 
								to[0].name, 
								to[0].address,
								to[1].name, 
								to[1].address,
								subject
							]
						) ;
						recipients.push(from);
						var allRecipients = recipients.concat(config.notify.sparkpost.recipients);
						// console.dir({from:config.referron.replyTo});		
						// console.dir({to:recipients});		
						var subject = 'Referral Not Recorded: ' + subject ;
						// console.dir({subject:subject});		
						// console.dir({confirmation:confirmation});		
						sendSparkPostMessage(sparkPost, config.referron.replyTo, subject, confirmation, allRecipients);				
						// console.log('... sending Referral Not Recorded finish ... ');
					} 
					else 
					{	
						request({
							url: webhook.url,
							method: webhook.method,
							json: webhook.body,
							headers: {
							    'Authorization-Token': referron['authorization-token']
							}
						}, function(error, response, body) {

							if (error) {

								console.dir({
									error: error
								});
								sendSparkPostMessage(sparkPost, config.notify.sparkpost.from, 'Unexpected error', JSON.stringify({
									error: error
								}), config.notify.sparkpost.recipients);

							} 
							else 
							{

								//console.dir(request);
								if (response.statusCode >= 400) {

									/* send email to administrator */
									console.dir({
										error: response.statusCode
									});
									sendSparkPostMessage(sparkPost, config.notify.sparkpost.from, 'Unexpected error', body, config.notify.sparkpost.recipients);

								} else {

									//console.dir({
									//	response: response
										//});

									// console.log('... body.start ... ');
									console.log(response.statusCode);
									//console.dir(JSON.stringify(body));
									//console.log('... body.end ... ');

									if( body.senderUrl ) {
										if ( body.request ) {
											console.log('... sending confirmation start ... ');
											console.log(body.senderUrl);		
											var recipients = [] ;		
											var htmlTemplate = "Hi %s<br/><br/>Your referral of %s %s to %s %s has been recorded.<br/><br/>You can view the referral at %s<br/><br/>Regards<br/><br/>Your Referral Team at http://referron.com";
											var confirmation = vsprintf(htmlTemplate, 
												[	body.request.from.name, 
													body.request.to[0].name, 
													body.request.to[0].address,
													body.request.to[1].name, 
													body.request.to[1].address,
													body.senderUrl
												]
											) ;
											recipients.push(body.request.from);
											//console.dir({from:config.referron.replyTo});		
											//console.dir({to:recipients});		
											var subject = 'Referral Recorded: ' + body.request.subject ;
											//console.dir({subject:subject});		
											//console.dir({confirmation:confirmation});		
											sendSparkPostMessage(sparkPost, config.referron.replyTo, subject, confirmation, recipients);				
											//console.log('... sending confirmation finish ... ');
										}
									}
								}

							}
				
						}); // request
					}
			
				} // test that seeker address does not equal provider address 
			} // seekerIndex
		} // providerIndex
	
	} // m.to
	else {
		
		// console.log('... sending Referral Not Recorded start ... ');			
		var recipients = [] ;		
		var htmlTemplate = "Hi %s<br/><br/>Your referral with the subject %s has <b>NOT</b> been recorded because it has no recipients in the to field. <b>Please send again putting providers in the to field, seekers in the cc field and referral@referron.com in the bcc field.</b>.<br/><br/>Regards<br/><br/>Your Referral Team at http://referron.com";
		var confirmation = vsprintf(htmlTemplate, 
			[	m.from[0].name, 
				subject
			]
		) ;
		recipients.push(m.from[0]);
		var allRecipients = recipients.concat(config.notify.sparkpost.recipients);
		//console.dir({from:config.referron.replyTo});		
		//console.dir({to:recipients});		
		var subject = 'Referral Not Recorded: ' + m.subject ;
		//console.dir({subject:subject});		
		//console.dir({confirmation:confirmation});		
		sendSparkPostMessage(sparkPost, config.referron.replyTo, subject, confirmation, allRecipients);				
		//console.log('... sending Referral Not Recorded finish ... ');
		
	}
	
}



validMXDomain('administrator@' + config.domain, function(validation, addresses, err) {
	if (validation) {
		for (var addressIndex = 0; addressIndex < addresses.length; addressIndex++) {
			var address = addresses[addressIndex];
			if (!mailHost) {
				mailHost = address;
			} else {
				if (mailHost.priority > address.priority) {
					mailHost = address;
				}
			}
		}
	} else {
		console.log('Error: ' + err);
	}
	mailHost = mailHost.exchange;

	if (mailHost) {

		var imap = {
			user: config.email,
			password: config.password,
			host: config.host,
			port: config.port,
			tls: config.tls,
			tlsOptions: config.tlsOptions
		};

		var referron = {
			email: config.referron.email,
			password: config.referron.password,
			url: config.referron.url
		};

		referron.imap = imap ;
		
		console.log( dateFormatAsString(new Date(), 'yyyyMMdd.hhmm') + ': attempting logon ');
		logonToReferron(referron, logonToReferronProcess );

	} else {
		console.log(' no host for domain ' + config.domain);
	}
	// console.dir(mailHost) ;
});
